package com.stackoverflowusers.ui.users.list

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.stackoverflowusers.TrampolineSchedulerRule
import com.stackoverflowusers.data.UsersRepository
import com.stackoverflowusers.data.model.User
import io.reactivex.Observable
import org.junit.Before

import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when`


class UserListPresenterTest {

    @get:Rule
    val rule = TrampolineSchedulerRule()

    private lateinit var userListPresenter: UserListPresenter
    private lateinit var mockUsersRepository: UsersRepository
    private lateinit var mockUserListView: UserListView

    @Before
    fun setUp() {
        mockUsersRepository = mock()
        mockUserListView = mock()
        userListPresenter = UserListPresenter(mockUsersRepository)
        userListPresenter.attachView(mockUserListView)
    }


    @Test
    fun presenterSuccess() {
        val userList = mutableListOf<User>()
        userList.add(User(1L, "imgUrl", "Name Surname", "London", 123, 1, false, false))

        `when`(mockUsersRepository.getTop20Users()).thenReturn(Observable.just(userList))

        userListPresenter.loadTop20Users()
        verify(mockUserListView, times(1)).userListProgress()
        verify(mockUserListView, times(1)).userListLoaded(userList)
        verify(mockUserListView, times(0)).userListError()
    }

    @Test
    fun presenterError() {
        `when`(mockUsersRepository.getTop20Users()).thenReturn(Observable.error(RuntimeException("test")))
        userListPresenter.loadTop20Users()
        verify(mockUserListView, times(1)).userListProgress()
        verify(mockUserListView, times(0)).userListLoaded(any())
        verify(mockUserListView, times(1)).userListError()
    }

}