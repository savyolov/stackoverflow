package com.stackoverflowusers.data

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.stackoverflowusers.TrampolineSchedulerRule
import com.stackoverflowusers.data.local.UserDb
import com.stackoverflowusers.data.model.User
import com.stackoverflowusers.data.model.UsersResponse
import com.stackoverflowusers.data.remote.UserService
import io.reactivex.Observable
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito


class UsersRepositoryTest {

    @get:Rule
    val rule = TrampolineSchedulerRule()

    private lateinit var usersRepository: UsersRepository
    private lateinit var mockUserService: UserService
    private lateinit var mockUserDb: UserDb

    @Before
    fun setUp() {
        mockUserService = mock()
        mockUserDb = mock()
        usersRepository = UsersRepository(mockUserService, mockUserDb)
    }

    @Test
    fun getUsersFromRepo() {
        Mockito.`when`(mockUserDb.getBlockedUserIds()).thenReturn(Observable.just(setOf(1L, 4L)))
        Mockito.`when`(mockUserDb.getFollowedUserIds()).thenReturn(Observable.just(setOf(2L, 4L)))

        val userList = mutableListOf<User>()
        userList.add(User(1L, "imgUrl", "Name1", "London", 123, 1, false, false))
        userList.add(User(2L, "imgUrl", "Name2", "Tokyo", 123, 1, false, false))
        userList.add(User(3L, "imgUrl", "Name2", "Madrid", 123, 1, false, false))
        userList.add(User(4L, "imgUrl", "Name3", "New York", 123, 1, false, false))
        Mockito.`when`(mockUserService.getUsers(any(), any(), any(), any())).thenReturn(Observable.just(UsersResponse(userList)))

        val list = usersRepository.getTop20Users().blockingFirst()

        assertThat(list[0].isBlocked, `is`(true))
        assertThat(list[0].isFollowed, `is`(false))

        assertThat(list[1].isBlocked, `is`(false))
        assertThat(list[1].isFollowed, `is`(true))

        assertThat(list[2].isBlocked, `is`(false))
        assertThat(list[2].isFollowed, `is`(false))

        assertThat(list[3].isBlocked, `is`(true))
        assertThat(list[3].isFollowed, `is`(true))
    }
}