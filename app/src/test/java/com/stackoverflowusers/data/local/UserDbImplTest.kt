package com.stackoverflowusers.data.local

import android.content.SharedPreferences
import com.nhaarman.mockito_kotlin.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`


class UserDbImplTest {

    private lateinit var userDb: UserDb
    private lateinit var mockSharedPreferences: SharedPreferences
    private lateinit var mockEditor: SharedPreferences.Editor

    @Before
    fun setUp() {
        mockSharedPreferences = mock()
        mockEditor = mock()
        userDb = UserDbImpl(mockSharedPreferences)
        `when`(mockSharedPreferences.edit()).thenReturn(mockEditor)
        `when`(mockEditor.putStringSet(any(), any())).thenReturn(mockEditor)
    }

    @Test
    fun testBlock() {
        `when`(mockSharedPreferences.getStringSet(UserDbImpl.BLOCK_KEY, emptySet<String>())).thenReturn(emptySet<String>())
        userDb.blockUnblockUser(1)
        verify(mockSharedPreferences, times(1)).edit()
        verify(mockEditor, times(1)).putStringSet(UserDbImpl.BLOCK_KEY, setOf("1"))
    }

    @Test
    fun testUnblock() {
        val userIdSet = setOf("1")
        `when`(mockSharedPreferences.getStringSet(UserDbImpl.BLOCK_KEY, emptySet<String>())).thenReturn(userIdSet)
        userDb.blockUnblockUser(1)
        verify(mockSharedPreferences, times(1)).edit()
        verify(mockEditor, times(1)).putStringSet(UserDbImpl.BLOCK_KEY, emptySet())
    }

    @Test
    fun testFollow() {
        `when`(mockSharedPreferences.getStringSet(UserDbImpl.FOLLOW_KEY, emptySet<String>())).thenReturn(emptySet<String>())
        userDb.followUnfollowUser(1)
        verify(mockSharedPreferences, times(1)).edit()
        verify(mockEditor, times(1)).putStringSet(UserDbImpl.FOLLOW_KEY, setOf("1"))
    }

    @Test
    fun testUnfollow() {
        val userIdSet = setOf("1")
        `when`(mockSharedPreferences.getStringSet(UserDbImpl.FOLLOW_KEY, emptySet<String>())).thenReturn(userIdSet)
        userDb.followUnfollowUser(1)
        verify(mockSharedPreferences, times(1)).edit()
        verify(mockEditor, times(1)).putStringSet(UserDbImpl.FOLLOW_KEY, emptySet())
    }
}