package com.stackoverflowusers

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.stackoverflowusers.di.AppComponent
import com.stackoverflowusers.di.AppModule
import com.stackoverflowusers.di.DaggerAppComponent
import timber.log.Timber

class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}