package com.stackoverflowusers.data.remote

import com.stackoverflowusers.data.model.UsersResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface UserService {

    @GET("/users")
    fun getUsers(@Query("pagesize") pagesize: Long,
                 @Query("order") order: String,
                 @Query("sort") sort: String,
                 @Query("site") site: String): Observable<UsersResponse>

}