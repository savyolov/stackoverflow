package com.stackoverflowusers.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class User(@SerializedName("account_id") val accountId: Long,
                @SerializedName("profile_image") val profileImage: String,
                @SerializedName("display_name") val displayName: String,
                @SerializedName("location") val location: String?,
                @SerializedName("creation_date") val creation_date: Long,
                @SerializedName("reputation") val reputation: Long,
                var isBlocked: Boolean,
                var isFollowed: Boolean) : Parcelable {

    constructor(source: Parcel) : this(
            source.readLong(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readLong(),
            source.readLong(),
            1 == source.readInt(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(accountId)
        writeString(profileImage)
        writeString(displayName)
        writeString(location)
        writeLong(creation_date)
        writeLong(reputation)
        writeInt((if (isBlocked) 1 else 0))
        writeInt((if (isFollowed) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<User> = object : Parcelable.Creator<User> {
            override fun createFromParcel(source: Parcel): User = User(source)
            override fun newArray(size: Int): Array<User?> = arrayOfNulls(size)
        }
    }
}