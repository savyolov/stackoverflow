package com.stackoverflowusers.data.model

import com.google.gson.annotations.SerializedName


data class UsersResponse(@SerializedName("items") val users: List<User>)