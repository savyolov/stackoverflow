package com.stackoverflowusers.data

import com.stackoverflowusers.data.local.UserDb
import com.stackoverflowusers.data.model.User
import com.stackoverflowusers.data.remote.UserService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers


class UsersRepository(private val userService: UserService, private val userDb: UserDb) {

    fun getTop20Users(): Observable<List<User>> {
        val blockedUserIdSetObservable = userDb.getBlockedUserIds()
        val followedUserIdSetObservable = userDb.getFollowedUserIds()

        val userListObservable = userService.getUsers(20, "desc", "reputation", "stackoverflow")
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.computation())
                .map { userResponse ->
                    return@map userResponse.users
                }

        return Observable.zip(
                blockedUserIdSetObservable,
                followedUserIdSetObservable,
                userListObservable,
                Function3<Set<Long>, Set<Long>, List<User>, List<User>> { blockedUserSet, followedUserIdSet, userList ->
                    for (user in userList) {
                        user.isBlocked = blockedUserSet.contains(user.accountId)
                        user.isFollowed = followedUserIdSet.contains(user.accountId)
                    }
                    return@Function3 userList
                })
    }

    fun blockUnblockUser(userId: Long): Completable {
        return userDb.blockUnblockUser(userId)
    }

    fun followUnfollowUser(userId: Long): Completable {
        return userDb.followUnfollowUser(userId)
    }
}