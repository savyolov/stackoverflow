package com.stackoverflowusers.data.local

import android.content.SharedPreferences
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * Temporary persistence layer implementation using shared preferences
 * Not for live release
 */
class UserDbImpl(private val preferences: SharedPreferences) : UserDb {

    override fun blockUnblockUser(userId: Long): Completable {
        val blockedUserSet = preferences.getStringSet(BLOCK_KEY, emptySet()).toMutableSet()
        val userIdString = userId.toString()
        if (blockedUserSet.contains(userIdString)) {
            blockedUserSet.remove(userIdString)
        } else {
            blockedUserSet.add(userIdString)
        }
        preferences.edit().putStringSet(BLOCK_KEY, blockedUserSet).apply()
        return Completable.complete()
    }

    override fun getBlockedUserIds(): Observable<Set<Long>> {
        val blockedUserStringSet = preferences.getStringSet(BLOCK_KEY, emptySet())
        val blockedUserSet = blockedUserStringSet.map { it.toLong() }.toSet()
        return Observable.just(blockedUserSet)
    }

    override fun followUnfollowUser(userId: Long): Completable {
        val followedUserIdSet = preferences.getStringSet(FOLLOW_KEY, emptySet()).toMutableSet()
        val userIdString = userId.toString()
        if (followedUserIdSet.contains(userIdString)) {
            followedUserIdSet.remove(userIdString)
        } else {
            followedUserIdSet.add(userIdString)
        }
        preferences.edit().putStringSet(FOLLOW_KEY, followedUserIdSet).apply()
        return Completable.complete()
    }

    override fun getFollowedUserIds(): Observable<Set<Long>> {
        val followedUserStringSet = preferences.getStringSet(FOLLOW_KEY, emptySet())
        val followedUserSet = followedUserStringSet.map { it.toLong() }.toSet()
        return Observable.just(followedUserSet)
    }

    companion object {
        const val BLOCK_KEY = "block"
        const val FOLLOW_KEY = "follow"
    }
}