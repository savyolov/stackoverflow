package com.stackoverflowusers.data.local

import io.reactivex.Completable
import io.reactivex.Observable

interface UserDb {

    fun blockUnblockUser(userId: Long) : Completable

    fun getBlockedUserIds(): Observable<Set<Long>>

    fun followUnfollowUser(userId: Long) : Completable

    fun getFollowedUserIds(): Observable<Set<Long>>

}