package com.stackoverflowusers.di

import com.stackoverflowusers.ui.users.list.UserListFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(fragment: UserListFragment)

}