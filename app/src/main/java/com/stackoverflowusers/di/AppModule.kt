package com.stackoverflowusers.di

import android.content.Context
import android.content.SharedPreferences
import com.stackoverflowusers.BuildConfig
import com.stackoverflowusers.data.UsersRepository
import com.stackoverflowusers.data.local.UserDb
import com.stackoverflowusers.data.local.UserDbImpl
import com.stackoverflowusers.data.remote.UserService
import com.stackoverflowusers.ui.users.list.UserListPresenter
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder().client(okHttpClient).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl("http://api.stackexchange.com/2.2/").build()

    @Provides
    @Singleton
    fun provideUserService(retrofit: Retrofit): UserService = retrofit.create(UserService::class.java)

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences {
        return context.getSharedPreferences("storage", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideUserDb(sharedPreferences: SharedPreferences): UserDb = UserDbImpl(sharedPreferences)

    @Provides
    @Singleton
    fun provideUsersRepository(userService: UserService, userDb: UserDb): UsersRepository = UsersRepository(userService, userDb)

    @Provides
    @Singleton
    fun provideUserListPresenter(repository: UsersRepository): UserListPresenter = UserListPresenter(repository)

}