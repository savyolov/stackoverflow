package com.stackoverflowusers.ui.common

import io.reactivex.disposables.CompositeDisposable

open class BasePresenter<T : MvpView> : Presenter<T> {

    protected lateinit var view: T
    protected lateinit var compositeDisposable: CompositeDisposable

    override fun attachView(view: T) {
        compositeDisposable = CompositeDisposable()
        this.view = view
    }

    override fun detachView() {
        compositeDisposable.dispose()
    }
}
