package com.stackoverflowusers.ui.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.stackoverflowusers.R
import kotlinx.android.synthetic.main.view_error.view.*


class ErrorView : FrameLayout {


    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_error, this)
        isClickable = true
    }

    fun setErrorMessageText(error: String) {
        component_error_message_tv.text = error
    }

    fun setRetryText(error: String) {
        component_error_retry_tv.text = error
    }

    fun setRetryAction(action: View.OnClickListener?) {
        if (null == action) {
            component_error_retry_tv.visibility = View.GONE
        } else {
            component_error_retry_tv.visibility = View.VISIBLE
            component_error_retry_tv.setOnClickListener(action)
        }
    }

}