package com.stackoverflowusers.ui.common

interface Presenter<in T : MvpView> {

    fun attachView(view: T)

    fun detachView()

}
