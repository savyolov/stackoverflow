package com.stackoverflowusers.ui.users

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.stackoverflowusers.R
import com.stackoverflowusers.data.model.User
import com.stackoverflowusers.ui.users.details.UserDetailsFragment
import com.stackoverflowusers.ui.users.list.UserListFragment
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity(), UserListFragment.OnListFragmentItemClickListener {

    private lateinit var onBackStackChangedListener: FragmentManager.OnBackStackChangedListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, UserListFragment.newInstance())
                    .commit()
        }
    }

    override fun onDestroy() {
        supportFragmentManager.removeOnBackStackChangedListener(onBackStackChangedListener)
        super.onDestroy()
    }

    override fun onListFragmentInteraction(user: User) {
        if (!user.isBlocked) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, UserDetailsFragment.newInstance(user))
                    .addToBackStack(null)
                    .commit()
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        onBackStackChangedListener = FragmentManager.OnBackStackChangedListener {
            val isMoreThanOneFragment = supportFragmentManager.backStackEntryCount > 0
            supportActionBar?.setDisplayHomeAsUpEnabled(isMoreThanOneFragment)
            supportActionBar?.setDisplayShowHomeEnabled(isMoreThanOneFragment)
        }
        supportFragmentManager.addOnBackStackChangedListener(onBackStackChangedListener)
    }
}
