package com.stackoverflowusers.ui.users.list

import com.stackoverflowusers.data.model.User
import com.stackoverflowusers.ui.common.MvpView

interface UserListView : MvpView {

    fun userListProgress()

    fun userListLoaded(userList: List<User>)

    fun userListError()

}