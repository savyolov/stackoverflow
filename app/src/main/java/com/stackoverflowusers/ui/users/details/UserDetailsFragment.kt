package com.stackoverflowusers.ui.users.details


import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.stackoverflowusers.R
import com.stackoverflowusers.data.model.User
import kotlinx.android.synthetic.main.fragment_user_details.*
import java.text.SimpleDateFormat
import java.util.*


class UserDetailsFragment : Fragment() {

    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = arguments!!.getParcelable(ARG_USER)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUserData()
    }

    private fun initUserData() {
        Picasso.with(context).load(user.profileImage).resizeDimen(R.dimen.image_size_details, R.dimen.image_size_details).into(user_image)
        user_name.text = getString(R.string.fragment_user_details_name, user.displayName)
        user_id.text = getString(R.string.fragment_user_details_id, user.accountId.toString())
        user_reputation.text = getString(R.string.fragment_user_details_reputation, user.reputation.toString())
        if (user.location != null) {
            user_location.text = getString(R.string.fragment_user_details_location, user.location)
        } else {
            user_location.visibility = View.GONE
        }
        user_creation_date.text = getString(R.string.fragment_user_details_creation_date, millisToDateString(user.creation_date))

        val resId = if (user.isFollowed) R.color.follow_selected else R.color.follow_unselected
        user_followed.setColorFilter(ContextCompat.getColor(context!!, resId), PorterDuff.Mode.SRC_IN)
    }

    private fun millisToDateString(millis: Long): String {
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return simpleDateFormat.format(calendar.time)
    }

    companion object {
        private const val ARG_USER = "ARG_USER"

        fun newInstance(user: User): UserDetailsFragment {
            val fragment = UserDetailsFragment()
            val args = Bundle()
            args.putParcelable(ARG_USER, user)
            fragment.arguments = args
            return fragment
        }
    }
}
