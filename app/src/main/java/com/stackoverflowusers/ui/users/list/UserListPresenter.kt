package com.stackoverflowusers.ui.users.list

import com.stackoverflowusers.data.UsersRepository
import com.stackoverflowusers.data.model.User
import com.stackoverflowusers.ui.common.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class UserListPresenter(private val repository: UsersRepository) : BasePresenter<UserListView>() {

    fun loadTop20Users() {
        view.userListProgress()
        compositeDisposable.add(repository.getTop20Users()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<List<User>>() {
                    override fun onNext(userList: List<User>) {
                        Timber.d("loadTop20Users onNext: " + userList)
                        view.userListLoaded(userList)
                    }

                    override fun onError(e: Throwable) {
                        view.userListError()
                    }

                    override fun onComplete() {
                    }
                }))
    }

    fun blockUnblockUser(userId: Long) {
        repository.blockUnblockUser(userId)
    }

    fun followUnfollowUser(userId: Long) {
        repository.followUnfollowUser(userId)
    }
}