package com.stackoverflowusers.ui.users.list

import android.content.Context
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.stackoverflowusers.R
import com.stackoverflowusers.data.model.User
import com.stackoverflowusers.ui.users.list.UserListFragment.OnListFragmentItemClickListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_user_list_item.view.*

class UserRecyclerViewAdapter(
        context: Context,
        private val clickListener: OnListFragmentItemClickListener?,
        private val onUserFollowListener: (Long) -> Unit,
        private val onUserBlockListener: (Long) -> Unit) : RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder>() {

    private var userList = emptyList<User>()

    private val blockedColor: Int = ContextCompat.getColor(context, R.color.block_selected)
    private val unblockedColor: Int = ContextCompat.getColor(context, R.color.block_unselected)
    private val followedColor: Int = ContextCompat.getColor(context, R.color.follow_selected)
    private val unfollowedColor: Int = ContextCompat.getColor(context, R.color.follow_unselected)
    private val selectableBackgroundResource: Int

    init {
        val attrs = intArrayOf(R.attr.selectableItemBackground)
        val typedArray = context.obtainStyledAttributes(attrs)
        selectableBackgroundResource = typedArray.getResourceId(0, 0)
        typedArray.recycle()
    }

    fun setItems(userList: List<User>) {
        this.userList = userList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_user_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userList[position]
        holder.bind(user)

        holder.containerView?.setOnClickListener {
            clickListener?.onListFragmentInteraction(user)
        }

        holder.containerView?.block?.setOnClickListener {
            user.isBlocked = !user.isBlocked
            notifyItemChanged(position)
            onUserBlockListener(user.accountId)
        }

        holder.containerView?.follow?.setOnClickListener {
            user.isFollowed = !user.isFollowed
            notifyItemChanged(position)
            onUserFollowListener(user.accountId)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    inner class ViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(user: User) {
            containerView?.let {
                val context = containerView.context
                containerView.user_name_reputation?.text = context.getString(R.string.fragment_user_list_item_user_name_reputation, user.displayName, user.reputation.toString())
                Picasso.with(context).load(user.profileImage).resizeDimen(R.dimen.user_list_image_size, R.dimen.user_list_image_size).into(containerView.user_image)
                setItemBackground(user, containerView)
                setBlockImageColor(user, containerView)
                setFollowImageColor(user, containerView)
            }
        }

        private fun setItemBackground(user: User, containerView: View) {
            if (user.isBlocked) {
                containerView.overlay_frame.setBackgroundResource(R.color.overlay_color)
            } else {
                containerView.overlay_frame.setBackgroundResource(selectableBackgroundResource)
            }
        }

        private fun setBlockImageColor(user: User, containerView: View) {
            val color = if (user.isBlocked) blockedColor else unblockedColor
            containerView.block.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        }

        private fun setFollowImageColor(user: User, containerView: View) {
            val color = if (user.isFollowed) followedColor else unfollowedColor
            containerView.follow.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        }
    }
}
