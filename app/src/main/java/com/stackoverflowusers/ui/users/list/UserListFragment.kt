package com.stackoverflowusers.ui.users.list

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.stackoverflowusers.App
import com.stackoverflowusers.R
import com.stackoverflowusers.data.model.User
import kotlinx.android.synthetic.main.fragment_user_list.*
import javax.inject.Inject
import android.support.v7.widget.SimpleItemAnimator


class UserListFragment : Fragment(), UserListView {

    @Inject
    lateinit var presenter: UserListPresenter

    private var itemClickListener: OnListFragmentItemClickListener? = null
    private lateinit var userRecyclerViewAdapter: UserRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as App).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initErrorView()
        presenter.attachView(this)
        presenter.loadTop20Users()
    }

    private fun initErrorView() {
        error_view.setErrorMessageText(getString(R.string.fragment_user_list_error_message))
        error_view.setRetryText(getString(R.string.fragment_user_list_error_retry))
        error_view.setRetryAction(View.OnClickListener {
            presenter.loadTop20Users()
        })
    }

    private fun initRecyclerView() {
        userRecyclerViewAdapter = UserRecyclerViewAdapter(context!!, itemClickListener, { presenter.followUnfollowUser(it) }, { presenter.blockUnblockUser(it) })
        user_list.adapter = userRecyclerViewAdapter
        user_list.setHasFixedSize(true)
        user_list.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        (user_list.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        user_list_swiperefresh.setOnRefreshListener({
            presenter.loadTop20Users()
        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnListFragmentItemClickListener) {
            itemClickListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnListFragmentItemClickListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        itemClickListener = null
    }

    override fun onDestroyView() {
        presenter.detachView()
        super.onDestroyView()
    }

    override fun userListProgress() {
        if (!user_list_swiperefresh.isRefreshing) {
            progress_bar.visibility = View.VISIBLE
            user_list_swiperefresh.visibility = View.GONE
            error_view.visibility = View.GONE
        }
    }

    override fun userListLoaded(userList: List<User>) {
        user_list_swiperefresh.isRefreshing = false
        progress_bar.visibility = View.GONE
        error_view.visibility = View.GONE

        user_list_swiperefresh.visibility = View.VISIBLE
        userRecyclerViewAdapter.setItems(userList)
    }

    override fun userListError() {
        user_list_swiperefresh.isRefreshing = false
        progress_bar.visibility = View.GONE
        user_list_swiperefresh.visibility = View.GONE
        error_view.visibility = View.VISIBLE
    }

    interface OnListFragmentItemClickListener {
        fun onListFragmentInteraction(user: User)
    }

    companion object {
        fun newInstance(): UserListFragment {
            return UserListFragment()
        }
    }
}
